#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

namespace Orf {

	const QString assetFilePath{"../ORF_2017/Assets/"};
	const QString shaderFilePath{"../ORF_2017/Shaders/"};

}

#endif // CONSTANTS_H
